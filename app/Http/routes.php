<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', [
    'uses'=>'PokemonController@indexView'
]);

Route::post('updatepokemon', [
    'uses'=>'PokemonController@submitPokemon'
]);

Route::get('user/{id}', [
	'uses' =>'PokemonController@pokemonView'
]);

Route::get('user/delete/{id}', [
	'uses' =>'PokemonController@deleteUser'
]);