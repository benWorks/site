<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Session;

use App\Pokemon;
use App\Account;


class PokemonController extends Controller
{

    public function submitPokemon(Request $req) {

        $username = $req->input('username');
        $password = $req->input('password');
        $pokemon = $req->input('pokemon');
        $level = $req->input('level');
        $xp = $req->input('xp');
        $xprequired = $req->input('xprequired');

        if(!$username) {
            return "Please enter a username";
        }

        $account = Account::where('username', '=', $username)->first();

        if(!$account) {
            $account = Account::create([
                'username' => $username,
                'password' => $password,
                'level' => $level,
                'xp' => $xp,
                'xprequired' => $xprequired,
            ]);
        }

    	$pokemondata = $req->input('pokemon');

        if(!$pokemondata) {
            return [ 'success' => true, 'pokemon' => 0 ];
        }

        $pokemondata = json_decode($pokemondata);
        $pokemonIDs = [];

        foreach($pokemondata as $pokemon) {
            $pokemonIDs[] = $pokemon->id;
        }

        $pokemonsToUpdate  = Pokemon::whereIn('pokemon_id', $pokemonIDs)->get();

        $dbIds = $pokemonsToUpdate->pluck('pokemon_id')->all();

        // dd($pokemonIDs);

        $pokemonsByAccount = Pokemon::where('account_id', $account->id)->get();

        $onesToAdd = array_diff($pokemonIDs, $dbIds);

        // dd($onesToAdd);

        $onesToDelete = array_diff($pokemonsByAccount->pluck('pokemon_id')->all(), $pokemonIDs);

        Pokemon::destroy($onesToDelete);

        foreach ($pokemondata as $pokemon) {
            if (!in_array($pokemon->id, $onesToAdd)) continue;

            Pokemon::create([
                'account_id' => $account->id,
                'pokemon_id' => $pokemon->id,
                'name' => $pokemon->name,
                'cp' => $pokemon->cp,
                'height' => $pokemon->height,
                'weight' => $pokemon->weight,
                'stamina' => $pokemon->stamina
            ]);
        }
 
        return [ 'success' => true ];

    }

    public function indexView() {

        $accounts = \App\Account::all();

        return view('basic.index')->with('users', $accounts);

    }

    public function pokemonView($id) {

        $pokemon = \App\Pokemon::where('account_id', '=', $id)->orderBy('CP', 'desc')->get();

        return view('basic.pokemon')->with('pokemondata', $pokemon)->with('id', $id)->with('count', $pokemon->count());
    }

    public function deleteUser($id) {
        $user = \App\Account::where('id', '=', $id)->first();

        $user->delete();

        return redirect('/');
    }
}
