<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pokemon extends Model
{
    protected $table = 'pokemon';

    protected $fillable = [
        'account_id', 'pokemon_id', 'name', 'cp', 'height', 'weight', 'stamina'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    //nothing goes here yet!
    ];
}
